//
//  GalleriesTableViewController.h
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-07-17.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

#import "AppDelegate.h"

@interface GalleriesTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *galleryBarButton;
@property (strong, nonatomic) AppDelegate *appDelegate;
@end
