//
//  LoginPage.h
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-04.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

CGFloat const verticalFBLoginAdjustmentFactor = 2.75;
CGFloat const verticalLogoAdjustmentfactor = 0.333;

@interface LoginPage : UIViewController 
@property (strong, nonatomic) UIImageView *cover;
@property (strong, nonatomic) FBSDKLoginButton *loginButton;
@property (strong, nonatomic) UIView *logoView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginBarButton;

@end
