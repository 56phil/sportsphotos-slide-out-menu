//
//  GalleriesPage.m
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-04.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

#import "GalleriesPage.h"

@interface GalleriesPage ()

@end

@implementation GalleriesPage

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.galleriesBarButton.target = self.revealViewController;
    self.galleriesBarButton.action = @selector(revealToggle:);
    
    self.webView.delegate = self;
    
    [self loadWebView];

}

- (void)orientationChanged:(NSNotification *)notification {
    [self webViewDidFinishLoad:self.webView];
}

- (void) loadWebView {
    self.urlRequest = [NSMutableURLRequest requestWithURL:
                       [NSURL URLWithString:@"http://www.sportsphotos.com/galleries/?noHeader=1"]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self.webView loadRequest:self.urlRequest];
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    });
}

- (void) webViewDidFinishLoad:(UIWebView *)aWebView {
    CGFloat scaleFactor = 1.0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGFloat scaleFactor = aWebView.frame.size.width / BASE_DEVICE_WIDTH;
        if (isPortrait) {
            scaleFactor *= 0.8675;
        } else {
            scaleFactor *= 0.6325;
        }
    } else {
        if (isPortrait) {
            scaleFactor *= 1.0;
        } else {
            scaleFactor *= 0.9125;
        }
    }
    
    aWebView.scalesPageToFit = YES;
    aWebView.contentMode = UIViewContentModeScaleAspectFill;
    aWebView.paginationMode = UIWebPaginationModeUnpaginated;
    aWebView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    
    NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = %f", scaleFactor];
    [aWebView stringByEvaluatingJavaScriptFromString:jsCommand];

    CGSize actualSize = aWebView.frame.size;
    UIScrollView *scroll=[aWebView scrollView];
    NSLog(@"\nactual size: %.0f, %.0f \tscroll view content size: %.0f, %.0f \tscale: %.5f",
          actualSize.width, actualSize.height,
          scroll.contentSize.width, scroll.contentSize.height, scaleFactor);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
