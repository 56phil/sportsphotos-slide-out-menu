//
//  MapViewController.m
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-03.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void) locationManager:(CLLocationManager *)manager
      didUpdateLocations:(NSArray *)locations {
    self.appDelegate.currentLocation = locations.lastObject;
    [self updateMap];
}

- (void) viewWillDisappear:(BOOL)animated {
    [self.appDelegate.currentLocationManager stopUpdatingLocation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.mapBarButton.target = self.revealViewController;
    self.mapBarButton.action = @selector(revealToggle:);

    self.latDelta = 0.025;
    self.lonDelta = 0.025;
    
    self.appDelegate.currentLocationManager.delegate = self;
}

- (void) viewDidAppear:(BOOL)animated {
    [self.appDelegate.currentLocationManager startUpdatingLocation];
    [self updateMap];
}

- (void) updateMap {
    self.latitude = self.appDelegate.currentLocation.coordinate.latitude;
    self.longitude = self.appDelegate.currentLocation.coordinate.longitude;
    self.span = MKCoordinateSpanMake(self.latDelta, self.lonDelta);
    self.location = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    self.region = MKCoordinateRegionMake(self.location, self.span);
    self.map.region = self.region;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
