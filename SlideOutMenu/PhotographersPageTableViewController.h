//
//  PhotographersPageTableViewController.h
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-04.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

@interface PhotographersPageTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *photographersBarButton;
@end
