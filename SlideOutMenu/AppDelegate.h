//
//  AppDelegate.h
//  SlideOutMenu
//
//  Created by Phil Huffman 8/3/15.
//  Copyright (c) 2015 ShortsPhotos.com. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) UIWindow *window;
@property CLLocationManager *currentLocationManager;
@property (strong, nonatomic) CLLocation* currentLocation;
@property (strong, nonatomic) NSArray *galleries;

- (void) doAlertWithtitle:(NSString*)title
               andMessage:(NSString*)message;
@end
