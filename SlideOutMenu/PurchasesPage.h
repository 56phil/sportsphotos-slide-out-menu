//
//  PurchasesPage.h
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-04.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

@interface PurchasesPage : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *purchasesBarButton;
@end
