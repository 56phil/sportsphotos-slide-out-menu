//
//  ViewController.m
//  SlideOutMenu
//
//  Created by Jared Davidson on 7/14/14.
//  Modified by Phil Huffman 8/3/15.
//  Copyright (c) 2014 Archetapp. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//-(void)viewWillAppear:(BOOL)animated {
//    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setUpGestures];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void) setUpGestures {
////    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//
//    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
//                                                                                 action:@selector(rightSwipeHandler)];
//    [self.rightSwipeGestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
//    [self.view addGestureRecognizer:self.rightSwipeGestureRecognizer];
//    self.rightSwipeGestureRecognizer.delegate = self;
//    
//    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
//                                                                                action:@selector(leftSwipeHandler)];
//    [self.leftSwipeGestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
//    [self.view addGestureRecognizer:self.leftSwipeGestureRecognizer];
//    self.leftSwipeGestureRecognizer.delegate = self;
//}

//- (void) rightSwipeHandler {
//    NSLog(@"Swipe right");
//}
//
//- (void) leftSwipeHandler {
//    NSLog(@"Swipe left");
//}

@end
