//
//  LoginPage.m
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-04.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

#import "LoginPage.h"

@interface LoginPage ()

@end

@implementation LoginPage

- (void)viewDidLoad {
    [super viewDidLoad];
    self.loginButton = [[FBSDKLoginButton alloc] init];
    self.cover = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sportsphotoslogo.png"]];
    
    self.loginBarButton.target = self.revealViewController;
    self.loginBarButton.action = @selector(revealToggle:);

    [self doLoginPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) positionFBLoginButton {
    CGPoint fbLoginButtonCenter = CGPointMake(self.view.center.x,
                                              self.view.frame.size.height -
                                              self.loginButton.frame.size.height * verticalFBLoginAdjustmentFactor);
    self.loginButton.center = fbLoginButtonCenter;
    [self.view addSubview:self.loginButton];
}

- (void) doLoginPage {
    [self positionFBLoginButton];
    [self positionLogo];
}

- (void) viewWillLayoutSubviews {
    if ([self.title isEqualToString:@"Login Page"]) {
        [self doLoginPage];
    }
}

- (void) positionLogo {
    CGFloat logoWidth = MIN(1.75 * 213.0, self.view.frame.size.width * 0.75);
    CGPoint logoCenter = CGPointMake(self.view.center.x, self.view.frame.size.height * verticalLogoAdjustmentfactor);
    self.cover.frame = CGRectMake(0.0, 0.0, logoWidth, logoWidth * 56.0 / 213.0);
    self.cover.center = logoCenter;
    [self.view addSubview:self.cover];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
