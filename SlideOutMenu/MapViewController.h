//
//  MapViewController.h
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-03.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "AppDelegate.h"

@interface MapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *mapBarButton;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;
@property (nonatomic) CLLocationDegrees latDelta;
@property (nonatomic) CLLocationDegrees lonDelta;
@property (nonatomic) MKCoordinateSpan span;
@property (nonatomic) CLLocationCoordinate2D location;
@property (nonatomic) MKCoordinateRegion region;
@end
