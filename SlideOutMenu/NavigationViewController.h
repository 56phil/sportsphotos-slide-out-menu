//
//  NavigationViewController.h
//  SlideOutMenu
//
//  Created by Jared Davidson on 7/14/14.
//  Modified by Phil Huffman 8/3/15.
//  Copyright (c) 2014 Archetapp. All rights reserved.
//

#import "AppDelegate.h"

@interface NavigationViewController : UITableViewController
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSArray *menu;
@end
