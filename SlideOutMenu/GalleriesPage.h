//
//  GalleriesPage.h
//  SlideOutMenu
//
//  Created by Philip Huffman on 2015-08-04.
//  Copyright (c) 2015 SportsPhotos.com. All rights reserved.
//

static const CGFloat BASE_DEVICE_WIDTH = 320.0;

@interface GalleriesPage : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *galleriesBarButton;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic)  NSMutableURLRequest *urlRequest;
@end
