//
//  AppDelegate.m
//  SlideOutMenu
//
//  Created Phil Huffman 8/3/15.
//  Copyright (c) 2014 Archetapp. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void) SetLocation {
    if ([CLLocationManager locationServicesEnabled]) {
        self.currentLocationManager = [[CLLocationManager alloc] init];
        self.currentLocationManager.delegate = self;
        self.currentLocationManager.distanceFilter = kCLDistanceFilterNone;
        self.currentLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8.0) {
            [self.currentLocationManager requestWhenInUseAuthorization];
        }
    } else {
        NSLog(@"Location services disabled.");
    }
}

-(void) locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    
    self.currentLocation = locations.lastObject;
}

- (NSArray*) getAllGalleries {
    NSDictionary *galleryJSONStructure = [self getGalleries];
    
    NSMutableArray *tempGalleries = [[NSMutableArray alloc] initWithArray:[galleryJSONStructure objectForKey:@"result"]];
    NSMutableArray *badDictionaries = [[NSMutableArray alloc] init];
    
    for (NSDictionary *gallery in tempGalleries) {
        if ([[gallery objectForKey:@"serverCloudId"] intValue] == 0) {
            [badDictionaries addObject:gallery];
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                 NSData *imageData = [NSData dataWithContentsOfURL:[[NSURL alloc] initWithString:[gallery objectForKey:@"default_photo_url"]]];
               dispatch_async(dispatch_get_main_queue(), ^{
                   if (imageData.length > 0) {
                       UIImage *tempImage = [[UIImage alloc] initWithData:imageData];
                       [gallery setValue:tempImage forKey:@"defaultPhotoImage"];
                       [gallery setValue:@YES forKey:@"defaultPhotoImagePresent"];
                   } else {
                       [gallery setValue:@NO forKey:@"defaultPhotoImagePresent"];

                   }
               });
            });
        }
    }
    
    [tempGalleries removeObjectsInArray:badDictionaries];
    
    return tempGalleries;
}

- (NSDictionary*) getGalleries {
    NSError *rawDataError;
    NSString *rawDataAddress = @"/Users/a56phil/dev/sp-OLD/albums.json";
    NSString *rawData = [[NSString alloc] initWithContentsOfFile:rawDataAddress
                                                        encoding:NSUTF8StringEncoding
                                                           error:&rawDataError];
    if (rawDataError) {
        NSLog(@"raw data error: %@", rawDataError.localizedFailureReason);
    } else {
        NSError *jsonError;
        NSData *objectData = [rawData dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        if (jsonError) {
            NSLog(@"json error: %@", jsonError.localizedFailureReason);
        } else {
            return json;
        }
    }
    return nil;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.galleries = [[NSArray alloc] initWithArray:[self getAllGalleries]];
    
    [self SetLocation];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void) doAlertWithtitle:(NSString*)title
               andMessage:(NSString*)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

//- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
//{
//    // Override point for customization after application launch.
//    return YES;
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

//- (void)applicationDidBecomeActive:(UIApplication *)application
//{
//    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
